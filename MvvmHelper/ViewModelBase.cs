﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MvvmHelper
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;

            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual void OnLoaded() { }

        public virtual void OnClosed() { }

        /// <summary>
        /// Locates corresponding View to ViewModel and calls Show method
        /// </summary>
        /// <param name="viewModel"></param>
        protected void ShowWindow(ViewModelBase viewModel)
        {
            ViewLocator.Default.ShowWindow(viewModel);
        }

        /// <summary>
        /// Locates corresponding View to ViewModel and calls ShowDialog method
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        protected bool? ShowDialogWindow(ViewModelBase viewModel)
        {
            return ViewLocator.Default.ShowDialogWindow(viewModel);
        }

        public event EventHandler Close;

        /// <summary>
        /// Closes the View
        /// </summary>
        public void CloseWindow()
        {
            if (Close != null)
                Close(this, EventArgs.Empty);
        }
    }
}
