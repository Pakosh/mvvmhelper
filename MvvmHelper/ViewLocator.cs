﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;

namespace MvvmHelper
{
    public sealed class ViewLocator
    {
        #region Singleton
        private static ViewLocator instance;
        /// <summary>
        /// Returns instance of ViewLocator in current AppDomain
        /// </summary>
        public static ViewLocator Default
        {
            get
            {
                if (instance == null)
                    instance = new ViewLocator();
                return instance;
            }
        }
        #endregion

        /// <summary>
        /// Mapped Typeof ViewModel to View
        /// </summary>
        private Dictionary<Type, Func<Window>> viewModelToWindow = new Dictionary<Type, Func<Window>>();

        static ViewLocator()
        {
            Thread.MemoryBarrier();
        }

        /// <summary>
        /// Registers ViewModel to View
        /// </summary>
        /// <typeparam name="VM"></typeparam>
        /// <typeparam name="W"></typeparam>
        public void RegisterWindow<VM, W>()
            where VM : ViewModelBase
            where W : Window, new()
        {
            viewModelToWindow.Add(typeof(VM), () => new W());
        }

        /// <summary>
        /// Returns corresponding View to ViewModel
        /// </summary>
        /// <param name="viewModelBase"></param>
        /// <returns></returns>
        public Window GetView(ViewModelBase viewModelBase)
        {
            return viewModelToWindow[viewModelBase.GetType()]();
        }

        /// <summary>
        /// Binds Window's DataContext, Loaded and Closed to ViewModel
        /// </summary>
        /// <param name="window"></param>
        /// <param name="viewModel"></param>
        public void BindViewModel(Window window, ViewModelBase viewModel)
        {
            window.DataContext = viewModel;
            window.Loaded += (o, e) => viewModel.OnLoaded();
            window.Closed += (o, e) => viewModel.OnClosed();
            viewModel.Close += (o, e) => window.Close();
        }

        /// <summary>
        /// Calls Show method to corresponding ViewModel's View
        /// </summary>
        /// <param name="viewModel"></param>
        public void ShowWindow(ViewModelBase viewModel)
        {
            var newWindow = GetView(viewModel);
            BindViewModel(newWindow, viewModel);
            newWindow.Show();
        }

        /// <summary>
        /// Calls ShowDialog method to corresponding ViewModel's View
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public bool? ShowDialogWindow(ViewModelBase viewModel)
        {
            var newWindow = GetView(viewModel);
            BindViewModel(newWindow, viewModel);
            return newWindow.ShowDialog();
        }
    }
}
