﻿using System;
using System.Windows.Input;

namespace MvvmHelper
{
    public sealed class RelayCommand : ICommand
    {
        private readonly ViewModelBase viewModel;
        private readonly Action execute;
        private readonly Func<bool> canExecute;

        /// <summary>
        /// RelayCommand Constructor with ViewModel, Execute and CanExecute
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="execute"></param>
        /// <param name="canExecute"></param>
        public RelayCommand(ViewModelBase viewModel, Action execute, Func<bool> canExecute = null)
        {
            this.viewModel = viewModel;
            this.execute = execute;
            this.canExecute = canExecute;

            if (canExecute != null)
                viewModel.PropertyChanged += (o, e) => RaiseCanExecuteChanged();
        }

        public bool CanExecute(object parameter)
        {
            return canExecute == null ? true : canExecute();
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            execute();
        }

        private void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;

            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
