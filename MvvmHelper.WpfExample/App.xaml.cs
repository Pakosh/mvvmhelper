﻿using MvvmHelper.WpfExample.ViewModels;
using MvvmHelper.WpfExample.Views;
using System.Windows;

namespace MvvmHelper.WpfExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            RegisterViews();
            ViewLocator.Default.ShowWindow(new MainViewModel());
        }

        private void RegisterViews()
        {
            var viewLocator = ViewLocator.Default;
            viewLocator.RegisterWindow<MainViewModel, MainWindow>();
        }
    }
}
