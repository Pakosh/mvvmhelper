﻿
namespace MvvmHelper.WpfExample.Models
{
    public class MockModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
    }
}
