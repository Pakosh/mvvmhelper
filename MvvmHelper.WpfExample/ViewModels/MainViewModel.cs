﻿using MvvmHelper.WpfExample.Classes;
using MvvmHelper.WpfExample.Models;
using System;
using System.Collections.ObjectModel;

namespace MvvmHelper.WpfExample.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<MockModel> mockModels;
        public ObservableCollection<MockModel> MockModels
        {
            get { return mockModels; }
            set
            {
                mockModels = value;
                OnPropertyChanged();
            }
        }

        private MockModel selectedMockModel;
        public MockModel SelectedMockModel
        {
            get { return selectedMockModel; }
            set
            {
                selectedMockModel = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand NewMockModel { get; set; }
        public RelayCommand EditMockModel { get; set; }
        public RelayCommand DeleteMockModel { get; set; }
        public RelayCommand UnSelectMockModel { get; set; }

        public MainViewModel()
        {
            mockModels = new ObservableCollection<MockModel>();

            Refresh();

            NewMockModel = new RelayCommand(this, New);
            EditMockModel = new RelayCommand(this, Edit, IsSelected);
            DeleteMockModel = new RelayCommand(this, Delete, IsSelected);
            UnSelectMockModel = new RelayCommand(this, Unselect, IsSelected);
        }

        private void Unselect()
        {
            SelectedMockModel = null;
        }

        private void Delete()
        {
            // TODO: Delete SelectedMockModel
            System.Diagnostics.Debug.WriteLine("DeleteCommand called.");
            MockModels.Remove(SelectedMockModel);
            SelectedMockModel = null;
        }

        private bool IsSelected()
        {
            return selectedMockModel != null;
        }

        private void Edit()
        {
            // TODO: Show MockModel View to edit SelectedMockModel
            System.Diagnostics.Debug.WriteLine("EditCommand called.");
        }

        private void New()
        {
            // TODO: Show MockModel View to create new MockModel
            System.Diagnostics.Debug.WriteLine("NewCommand called.");
        }

        private void Refresh()
        {
            MockModels.Clear();

            Random r = new Random();
            for (int i = 0; i < 10; i++)
                MockModels.Add(new MockModel
                    {
                        Id = i,
                        Comment = r.GetRandomString(20),
                        Name = r.GetRandomString(5),
                    });
        }

    }
}
