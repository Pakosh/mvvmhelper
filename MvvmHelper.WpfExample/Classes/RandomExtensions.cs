﻿using System;
using System.Linq;

namespace MvvmHelper.WpfExample.Classes
{
    public static class RandomExtensions
    {
        private static string alphabet = "abcdefg";
        public static string GetRandomString(this Random r, int length)
        {
            return new string(Enumerable.Range(0, length).Select(c => alphabet[r.Next(alphabet.Length)]).ToArray());
        }
    }
}
